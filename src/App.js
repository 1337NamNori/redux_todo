import React from 'react';
import {Card, CardActions, CardContent, Container, Divider} from "@mui/material";
import Header from "./components/Header";
import InputForm from "./components/InputForm";
import TodoList from "./components/TodoList";
import Footer from "./components/Footer";

function App() {
    return (
        <Container
            maxWidth="xs"
            sx={{
                padding: 0,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                height: '100vh',
            }}
        >
            <Card sx={{ width: '100%', minHeight: '100%', borderRadius: 0, position: 'relative'}}>
                <CardContent sx={{bgcolor: "primary.main",}}>
                    <Header />
                </CardContent>
                <Divider />
                <CardContent>
                    <InputForm />
                </CardContent>
                <Divider />
                <CardContent style={{overflowY: 'auto', maxHeight: 'calc(100vh - 254px)'}}>
                    <TodoList />
                </CardContent>
                <CardActions
                    sx={{
                        position: 'absolute',
                        bottom: 0, left: 0, right: 0,
                        height: 70,
                        bgcolor: 'primary.main',
                        padding: '16px'
                    }}
                >
                    <Footer />
                </CardActions>
            </Card>
        </Container>
    );
}

export default App;
