import {createSlice} from "@reduxjs/toolkit";
import * as storage from '../../storage';

const todos = storage.getTodos();

const todoSlice = createSlice({
    name: 'todo',
    initialState: {
        todos,
        loading: false,
        error: null,
    },
    reducers: {
        addTodo: (state, action) => {
            state.todos.unshift(action.payload);
            storage.setTodos(state.todos);
        },
        updateTodo: (state, action) => {
            state.todos = state.todos.map(todo => {
                if (todo.id === action.payload.id) {
                    todo.title = action.payload.title;
                }
                return todo;
            });
            storage.setTodos(state.todos);
        },
        removeTodo: (state, action) => {
            state.todos = state.todos.filter(todo => todo.id !== action.payload);
            storage.setTodos(state.todos);
        },
        toggleTodo: (state, action) => {
            state.todos = state.todos.map(todo => {
                if (todo.id === action.payload) {
                    todo.completed = !todo.completed;
                }
                return todo;
            });
            storage.setTodos(state.todos);
        },
        setLoading: (state, action) => {
            state.loading = action.payload;
        },
        setError: (state, action) => {
            state.error = action.payload;
        }
    }
});

export const {
    addTodo,
    removeTodo,
    toggleTodo,
    updateTodo,
    setLoading,
    setError
} = todoSlice.actions;

export const selectTodos = state => state.todo.todos;
export const selectError = state => state.todo.error;
export const selectLoading = state => state.todo.loading;

export default todoSlice.reducer;