import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import { v4 as uuidv4 } from 'uuid';
import {Grid, IconButton, TextField} from "@mui/material";
import {Add} from "@mui/icons-material";
import {addTodo} from "../features/todo/todoSlice";

function InputForm() {
    const dispatch = new useDispatch();
    const [value, setValue] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        if (value.trim() === '') return;

        dispatch(addTodo({
            id: uuidv4(),
            title: value,
            completed: false,
        }));
        setValue('');
    }

    return (
        <form onSubmit={handleSubmit}>
            <Grid container alignItems="center" justifyContent="space-between">
                <TextField
                    id="standard-basic"
                    label="New task"
                    variant="standard"
                    sx={{flexGrow: 1}}
                    value={value}
                    onInput={(e) => setValue(e.target.value)}
                />
                <IconButton color="primary" size="large" type="submit">
                    <Add />
                </IconButton>
            </Grid>
        </form>
    );
}

export default InputForm;