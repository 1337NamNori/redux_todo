import React from 'react';
import {Grid, Typography} from "@mui/material";
import Moment from "react-moment";
import {blueGrey} from "@mui/material/colors";
import {useSelector} from "react-redux";
import {selectTodos} from "../features/todo/todoSlice";

function Header() {
    const todos = useSelector(selectTodos);

    return (
        <Grid container alignItems="center" justifyContent="space-between">
            <div>
                <Typography variant="h4" color={blueGrey[50]}>
                    <Moment format="dddd" />
                    ,&nbsp;
                    <Moment format="Do" />
                </Typography>
                <Typography variant="subtitle1" color={blueGrey[100]}>
                    <Moment format="MMMM" />
                </Typography>
            </div>
            <div>
                <Typography color={blueGrey[50]}>
                    {todos.length} tasks
                </Typography>
            </div>
        </Grid>
    );
}

export default Header;