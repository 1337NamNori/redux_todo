import React from 'react';
import {Button, Card, Grid, Modal, Typography} from "@mui/material";
import {Delete, Edit} from "@mui/icons-material";
import {useDispatch} from "react-redux";
import {removeTodo} from "../../features/todo/todoSlice";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};

function DeleteModal({open, handleClose, id}) {
    const dispatch = useDispatch();

    const handleDelete = () => {
        dispatch(removeTodo(id));
        handleClose();
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Card sx={style}>
                <Typography align="center" variant="h4">Are you sure?</Typography>
                <Grid container justifyContent="center" mt={3}>
                    <Button color="error" variant="contained"
                            onClick={handleDelete}
                    >
                        <Edit sx={{mr: 1}}/>
                        Delete
                    </Button>
                    <Button variant="contained" color="inherit" sx={{marginLeft: 2}}
                            onClick={handleClose}
                    >
                        <Delete sx={{mr: 1}}/>
                        Cancel
                    </Button>
                </Grid>
            </Card>
        </Modal>
    );
}

export default DeleteModal;