import React, {useEffect, useState} from 'react';
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button, Checkbox,
    Grid,
    Input,
    Typography
} from "@mui/material";
import {Delete, Edit, ExpandMore, Save} from "@mui/icons-material";
import {blueGrey} from "@mui/material/colors";
import {toggleTodo, updateTodo} from "../../features/todo/todoSlice";
import {useDispatch} from "react-redux";

function TodoItem({title, completed, id, expanded, handleChange, onDeleteClick}) {
    const dispatch = useDispatch();

    const [editing, setEditing] = useState(false);
    const [editingTitle, setEditingTitle] = useState(title);

    useEffect(() => {
        if (expanded !== `panel-${id}`) setEditing(false);
    }, [expanded, id]);

    const handleUpdateTodo = () => {
        dispatch(updateTodo({id, title: editingTitle}));
        setEditing(false);
    }

    const cancelUpdate = () => {
        setEditing(false);
        setEditingTitle(title);
    }

    return (
        <Accordion expanded={expanded === `panel-${id}`} onChange={handleChange(`panel-${id}`)}>
            <AccordionSummary
                expandIcon={<ExpandMore/>}
                aria-controls="panel1a-content"
                id="panel1a-header"
            >
                <Checkbox
                    onClick={e => {e.stopPropagation();}}
                    checked={completed}
                    onChange={() => {dispatch(toggleTodo(id))}}
                />
                <div style={{display: 'block', width: '200px'}}>
                    {!editing ? (
                        <Typography
                            variant="subtitle1" mt={1} noWrap
                            color={completed ? blueGrey['A400'] : blueGrey['900']}
                            style={completed ? {textDecoration: 'line-through'} : {}}
                        >
                            {title}
                        </Typography>
                    ) : (
                        <Input
                            value={editingTitle}
                            onClick={e => e.stopPropagation()}
                            onChange={(e) => setEditingTitle(e.target.value)}
                            onKeyUp={(e) => {
                                if (e.key === 'Enter') handleUpdateTodo();
                                if (e.key === 'Escape') cancelUpdate();
                            }}
                        />
                    )}
                </div>
            </AccordionSummary>
            <AccordionDetails>
                <Grid container justifyContent="end">
                    {!editing ? (
                        <>
                            <Button color="primary" variant="contained"
                                    onClick={() => setEditing(true)}
                            >
                                <Edit sx={{mr: 1}}/>
                                Edit
                            </Button>
                            <Button color="error" variant="contained" sx={{marginLeft: 2}}
                                    onClick={onDeleteClick}
                            >
                                <Delete sx={{mr: 1}}/>
                                Delete
                            </Button>
                        </>
                    ) : (
                        <>
                            <Button color="primary" variant="contained"
                                    onClick={handleUpdateTodo}
                            >
                                <Save sx={{mr: 1}}/>
                                Save
                            </Button>
                            <Button variant="contained" color="inherit" sx={{marginLeft: 2}}
                                    onClick={cancelUpdate}
                            >
                                <Save sx={{mr: 1}}/>
                                Cancel
                            </Button>
                        </>
                    )}
                </Grid>
            </AccordionDetails>
        </Accordion>
    );
}

export default TodoItem;