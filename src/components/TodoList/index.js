import React, {useState} from 'react';
import TodoItem from "./TodoItem";
import {useSelector} from "react-redux";
import {selectTodos} from "../../features/todo/todoSlice";
import DeleteModal from "./DeleteModal";

function TodoList() {
    const [expanded, setExpanded] = useState(false);
    const [deleteId, setDeleteId] = useState(null);
    const [showDeleteModal, setShowDeleteModal] = useState(false);

    const todos = useSelector(selectTodos);

    const handleChange = panel => (e, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    }

    const openDeleteModal = (id) => {
        setDeleteId(id);
        setShowDeleteModal(true);
    }

    return (
        <div >
            {todos.map(todo => (
                <TodoItem
                    key={todo.id}
                    title={todo.title}
                    completed={todo.completed}
                    id={todo.id}
                    expanded={expanded}
                    handleChange={handleChange}
                    onDeleteClick={() => openDeleteModal(todo.id)}
                />
            ))}
            <DeleteModal
                id={deleteId}
                open={showDeleteModal}
                handleClose={() => setShowDeleteModal(false)}
            />
        </div>
    );
}

export default TodoList;