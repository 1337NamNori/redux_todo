import React, {useEffect, useState} from 'react';
import {LinearProgress, Typography} from "@mui/material";
import {blueGrey} from "@mui/material/colors";
import {useSelector} from "react-redux";
import {selectTodos} from "../features/todo/todoSlice";

function Footer() {
    const todos = useSelector(selectTodos);
    const [progress, setProgress] = useState(0);

    useEffect(() => {
        const completed = todos.filter(todo => todo.completed === true).length;
        const total = todos.length;
        setProgress(completed / total * 100);
    }, [todos]);

    return (
        <footer style={{width: '100%'}}>
            <Typography color={blueGrey[50]} variant="h5">
                Progress: {todos.filter(todo => todo.completed === true).length}/{todos.length}
            </Typography>
            <LinearProgress color="success" variant="determinate" value={progress} />
        </footer>
    );
}

export default Footer;