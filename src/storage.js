const TODOS = 'todos';

export const setTodos = (todos) => {
    localStorage.setItem(TODOS, JSON.stringify(todos));
}

export const getTodos = () => {
    const todos = localStorage.getItem(TODOS);
    return todos ? JSON.parse(todos) : [];
}